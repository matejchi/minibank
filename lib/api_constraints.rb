# frozen_string_literal: true

class ApiConstraints
  ACCEPT = 'ACCEPT'

  def initialize(options)
    @version = options[:version]
    @default = options[:default]
  end

  def matches?(req)
    if req.headers && req.headers[ACCEPT]
      @default || req.headers[ACCEPT].include?("application/vnd.mini_bank.v#{@version}")
    else
      @default
    end
  end
end
