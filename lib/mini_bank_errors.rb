# frozen_string_literal: true

module MiniBankErrors
  class Error < StandardError; end
  class UnsupportedCurrency < Error; end
  class NoAvailableFunds < Error; end
  class NotExternalUser < Error; end
end
