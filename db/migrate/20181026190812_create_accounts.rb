# frozen_string_literal: true

class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.string :account_no, null: false
      t.string :currency
      t.decimal :balance, precision: 14, scale: 3, null: false, default: 0.0
      t.references :user, foreign_key: true
      t.boolean :active, default: true
      t.string :account_type
      t.string :description

      t.timestamps
    end

    add_index :accounts, :account_no, unique: true
  end
end
