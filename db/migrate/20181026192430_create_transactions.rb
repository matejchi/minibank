# frozen_string_literal: true

class CreateTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions do |t|
      t.references :debtor
      t.references :creditor
      t.decimal :amount, precision: 14, scale: 3, null: false, default: 0.0
      t.string :currency
      t.string :transaction_type, null: false
      t.string :subject, null: false
      t.string :description
      t.string :status

      t.timestamps
    end

    add_foreign_key :transactions, :accounts, column: :debtor_id, primary_key: :id
    add_foreign_key :transactions, :accounts, column: :creditor_id, primary_key: :id
  end
end
