# frozen_string_literal: true

user = User.where(email: 'external_user@isp.net').first_or_initialize do |user|
  user.first_name = 'Chuck'
  user.last_name = 'Norris'
  user.email = 'external_user@isp.net'
  user.password = 'godmode'
  user.save!
end

return if user.account
user.create_account!(description: 'EXTERNAL ACCOUNT',
                     account_type: 'EXTERNAL',
                     currency: 'EUR')
