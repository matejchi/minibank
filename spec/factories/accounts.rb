# frozen_string_literal: true

FactoryBot.define do
  factory :account do
    account_no                { Faker::Number.unique.number(8) }
    currency                  { 'EUR' }
    account_type              { 'INTERNAL' }
    user

    trait :with_funds do
      balance { '3000' }
    end

    trait :external do
      account_type { 'EXTERNAL' }
      description { 'EXTERNAL ACCOUNT' }
      balance { '5000' }
    end
  end
end
