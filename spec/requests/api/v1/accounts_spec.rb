# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'API::V1::AccountsController' do
  describe 'GET api/accounts' do
    let(:headers) { { 'Content-Type' => 'application/json' } }
    let(:account) { create(:account) }

    context 'when request params are ok' do
      it 'returns user\'s account' do
        get '/api/accounts', headers: headers.merge('api-token': account.user.api_token, 'email': account.user.email)
        expect(response).to be_successful
        results = JSON.parse(response.body, symbolize_names: true)
        expect(results.keys).to match_array(%i[id account_no currency balance user_id active account_type
                                               description created_at updated_at])
      end
    end

    context 'when request headers are not ok' do
      it 'returns status code 401 when api-token header is wrong' do
        get '/api/accounts', headers: headers.merge('api-token': 'wrong header', 'email': account.user.email)
        expect(response).to be_unauthorized
      end

      it 'returns status code 400 when email header is missing' do
        get '/api/accounts', headers: headers.merge('api-token': account.user.api_token)
        expect(response).to be_bad_request
      end
    end
  end
end
