# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'API::V1::UsersController' do
  describe 'POST api/users' do
    let(:payload) do
      {
        user: {
          first_name: 'Matej',
          last_name: 'Cica',
          email: 'matej@isp.net',
          password: '0000',
          password_confirmation: '0000'
        }
      }
    end
    let(:headers) { { 'Content-Type' => 'application/json' } }

    context 'when request params are ok' do
      it 'creates new user' do
        post '/api/users', params: payload.to_json, headers: headers
        expect(response).to be_successful
        results = JSON.parse(response.body, symbolize_names: true)
        expect(results.keys).to match_array(%i[id first_name last_name email api_token
                                               token_expires_at active account])
        expect(response).to have_http_status(:success)
      end
    end

    context 'when request params are not ok' do
      it 'returns status code 422 when password is too short' do
        post '/api/users', params: payload.merge(user: { password: '000' }).to_json, headers: headers
        expect(response).to be_unprocessable
      end
    end
  end
end
