# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'API::V1::SessionsController' do
  describe 'POST api/sessions' do
    let(:headers) { { 'Content-Type' => 'application/json' } }
    let(:user) { create(:user) }

    context 'when request params are ok' do
      it 'logs in user' do
        params = {
          email: user.email,
          password: user.password
        }

        post '/api/sessions', params: params.to_json, headers: headers
        expect(response).to be_successful
        expect(response).to have_http_status(:success)
      end
    end

    context 'when request params are not ok' do
      it 'returns status code 404 when email is wrong' do
        post '/api/sessions', params: { email: 'blalba@isp.net', password: '0000' }.to_json, headers: headers
        expect(response).to have_http_status(:not_found)
      end

      it 'returns status code 400 when password is wrong' do
        post '/api/sessions', params: { email: user.email, password: '0020' }.to_json, headers: headers
        expect(response).to be_bad_request
      end
    end
  end

  describe 'DELETE api/sessions' do
    let(:headers) { { 'Content-Type' => 'application/json' } }
    let(:user) { create(:user) }

    context 'when request headers are ok' do
      it 'logs out user' do
        delete '/api/session', headers: headers.merge('api-token': user.api_token, 'email': user.email)
        expect(response).to be_successful
        expect(response).to have_http_status(:success)
      end
    end

    context 'when request headers are not ok' do
      it 'returns unauthorized status' do
        delete '/api/session', headers: headers.merge('api-token': 'blabla', 'email': user.email)
        results = JSON.parse(response.body, symbolize_names: true)
        expect(results[:message]).to eq('Token expired, or wrong credentials')
        expect(response).to be_unauthorized
      end
    end

    context 'when email request header is missing' do
      it 'logs out user' do
        delete '/api/session', headers: headers.merge('api-token': user.api_token)
        expect(response).to be_bad_request
      end
    end
  end
end
