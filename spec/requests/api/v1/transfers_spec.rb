# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'API::V1::TransfersController' do
  describe 'POST api/transfers' do
    let(:headers) { { 'Content-Type' => 'application/json' } }
    let(:debit_account) { create(:account, :with_funds) }
    let(:credit_account) { create(:account, :with_funds) }
    let(:req_params) do
      {
        transfer: {
          beneficary: {
            account_no: credit_account.account_no
          },
          description: 'blabla',
          subject: 'payment',
          transfer_type: 'INTERNAL',
          currency: 'EUR',
          amount: 100.234
        }
      }
    end
    let(:external_account) { create(:account, :external) }
    let(:external_user) { create(:user, :god_mode) }

    context 'when request params/headers are ok' do
      it 'creates new transfer' do
        post '/api/transfers', params: req_params.to_json,
                               headers: headers.merge('api-token': debit_account.user.api_token,
                                                      'email': debit_account.user.email)
        expect(response).to be_created
        results = JSON.parse(response.body, symbolize_names: true)
        expect(results[:message]).to eq('Transfer completed successfully')
      end

      it "subtracts debtor's balance by transfer amount" do
        debit_balance_before = debit_account.balance
        post '/api/transfers', params: req_params.to_json,
                               headers: headers.merge('api-token': debit_account.user.api_token,
                                                      'email': debit_account.user.email)
        expect(response).to be_created
        debit_account.reload
        expect(debit_account.balance).to eq(debit_balance_before - req_params.dig(:transfer, :amount))
      end

      it "adds to creditor's balance by transfer amount" do
        credit_balance_before = credit_account.balance
        post '/api/transfers', params: req_params.to_json,
                               headers: headers.merge('api-token': debit_account.user.api_token,
                                                      'email': debit_account.user.email)
        expect(response).to be_created
        credit_account.reload
        expect(credit_account.balance).to eq(credit_balance_before + req_params.dig(:transfer, :amount))
      end

      it 'will not transfer money if there is no enough funds' do
        req_params[:transfer][:amount] = 100_000
        post '/api/transfers', params: req_params.to_json,
                               headers: headers.merge('api-token': debit_account.user.api_token,
                                                      'email': debit_account.user.email)

        expect(response).to be_bad_request
        results = JSON.parse(response.body, symbolize_names: true)
        expect(results[:message]).to eq('No available funds')
      end
    end

    context 'when request params/headers are not ok' do
      it 'will not create transfer if currency is other than EUR' do
        req_params[:transfer][:currency] = 'USD'
        post '/api/transfers', params: req_params.to_json,
                               headers: headers.merge('api-token': debit_account.user.api_token,
                                                      'email': debit_account.user.email)
        expect(response).to be_bad_request
        results = JSON.parse(response.body, symbolize_names: true)
        expect(results[:message]).to eq('Currently only EUR currency is supported')
      end
    end

    context 'when transfer is external' do
      it 'does not create transfer if user is not external' do
        req_params[:transfer][:transfer_type] = 'external'
        post '/api/transfers', params: req_params.to_json,
                               headers: headers.merge('api-token': debit_account.user.api_token,
                                                      'email': debit_account.user.email)
        expect(response).to be_bad_request
        results = JSON.parse(response.body, symbolize_names: true)
        expect(results[:message]).to eq('Only EXTERNAL user can give credit to the user!')
      end

      it 'does create transfer if user is external and transfer type is external' do
        external_user.account = external_account
        req_params[:transfer][:transfer_type] = 'external'
        post '/api/transfers', params: req_params.to_json,
                               headers: headers.merge('api-token': external_user.api_token,
                                                      'email': external_user.email)
        expect(response).to be_successful
        results = JSON.parse(response.body, symbolize_names: true)
        expect(results[:message]).to eq('Transfer completed successfully')
      end

      it 'does not create transfer if transfer type is not external' do
        external_user.account = external_account
        req_params[:transfer][:transfer_type] = 'custom'
        post '/api/transfers', params: req_params.to_json,
                               headers: headers.merge('api-token': external_user.api_token,
                                                      'email': external_user.email)
        expect(response).to be_bad_request
      end
    end
  end
end
