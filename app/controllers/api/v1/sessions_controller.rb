# frozen_string_literal: true

class Api::V1::SessionsController < ApplicationController
  skip_before_action :authenticate_user, only: :create

  def create
    @user = User.find_by!(email: params[:email].downcase)
    @user.authenticate(params[:password]) ? render_user : render_error
  end

  def destroy
    User.find_by!(email: request.headers[EMAIL]).update_attributes!(token_expires_at: 1.hour.ago)
    head :ok
  end

  private

  def render_user
    @user.generate_new_token
    @user.save!
    render json: @user, except: %i[password_digest created_at updated_at], status: :ok
  end

  def render_error
    render json: { message: 'Please check your credentials' }, status: :bad_request
  end
end
