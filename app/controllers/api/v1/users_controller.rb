# frozen_string_literal: true

class Api::V1::UsersController < ApplicationController
  skip_before_action :authenticate_user, only: :create

  def create
    render json: create_user, except: %i[password_digest created_at updated_at], include: :account, status: :created
  end

  private

  def create_user
    Users::Create.new(create_user_params).call
  end

  def create_user_params
    params.require(:user).permit(:first_name,
                                 :last_name,
                                 :email,
                                 :password,
                                 :password_confirmation)
  end
end
