# frozen_string_literal: true

class Api::V1::TransactionsController < ApplicationController
  def credited
    render json: transactions('Credited'), status: :ok
  end

  def debited
    render json: transactions('Debited'), status: :ok
  end

  private

  def transactions(transaction)
    "Transactions::#{transaction}".constantize.new(current_user).call
  end
end
