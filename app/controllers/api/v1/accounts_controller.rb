# frozen_string_literal: true

class Api::V1::AccountsController < ApplicationController
  def index
    render json: account, status: :ok
  end

  private

  def account
    current_user.account
  end
end
