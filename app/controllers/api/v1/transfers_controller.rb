# frozen_string_literal: true

class Api::V1::TransfersController < ApplicationController
  def create
    create_transfer
    render json: { message: 'Transfer completed successfully' }, status: :created
  end

  private

  def create_transfer
    raise MiniBankErrors::UnsupportedCurrency if params.dig(:transfer, :currency)&.upcase != 'EUR'

    Transfers::Create.new(params: transfer_params, debtor: debtor, creditor: creditor).call
  end

  def transfer_params
    params.require(:transfer).permit(:description, :subject, :transfer_type, :currency, :amount,
                                     beneficary: %i[account_no email])
  end

  def debtor
    current_user
  end

  def creditor
    Account.find_by!(account_no: params.dig(:transfer, :beneficary, :account_no))
  end
end
