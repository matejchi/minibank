# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session

  API_TOKEN = 'Api-Token'
  EMAIL = 'email'

  rescue_from ActiveRecord::RecordNotFound,     with: :not_found
  rescue_from ActiveRecord::RecordInvalid,      with: :unprocessable_entity
  rescue_from NoMethodError,                    with: :bad_request
  rescue_from MiniBankErrors::UnsupportedCurrency, with: :unsupported_currency
  rescue_from MiniBankErrors::NoAvailableFunds, with: :no_available_funds
  rescue_from MiniBankErrors::NotExternalUser,  with: :not_external_user

  before_action :authenticate_user, :destroy_session

  def current_user
    @current_user ||= User.find_by!(api_token: request.headers[API_TOKEN])
  end

  private

  def not_found
    head :not_found
  end

  def unprocessable_entity(error)
    render json: ErrorSerializer.serialize(error.record.errors), status: :unprocessable_entity
  end

  def authenticate_user
    return bad_request unless request.headers[API_TOKEN] && request.headers[EMAIL]

    @user = User.find_by!(email: request.headers[EMAIL].downcase)
    return unauthorized unless token_valid?

    @current_user = @user
  end

  # disabling CSRF token and cookies
  def destroy_session
    request.session_options[:skip] = true
  end

  def bad_request
    head :bad_request
  end

  def token_valid?
    is_match? && unexpired?
  end

  def is_match?
    # instead of just comparing DB token with the one from the header, let's use 'secure compare'
    # to avoid timing attacks
    ActiveSupport::SecurityUtils.secure_compare(@user.api_token, request.headers[API_TOKEN])
  end

  def unexpired?
    @user.token_expires_at > Time.current
  end

  def unauthorized
    render json: { message: 'Token expired, or wrong credentials' }, status: :unauthorized
  end

  def unsupported_currency
    render json: { message: 'Currently only EUR currency is supported' }, status: :bad_request
  end

  def no_available_funds
    render json: { message: 'No available funds' }, status: :bad_request
  end

  def not_external_user
    render json: { message: 'Only EXTERNAL user can give credit to the user!' }, status: :bad_request
  end
end
