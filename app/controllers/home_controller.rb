# frozen_string_literal: true

class HomeController < ApplicationController
  skip_before_action :authenticate_user

  def index
    render json: { message: 'Hi! You are reaching MiniBank\'s API.' }, status: :ok
  end
end
