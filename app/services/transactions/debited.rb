# frozen_string_literal: true

module Transactions
  class Debited
    def initialize(user)
      @user = user
    end

    def call
      debited_transactions
    end

    private

    def debited_transactions
      # we could easily return transactions by doing just this:
      #   @user.account.debited_transactions
      # but that will trigger 3 additional SQL queries against DB (n+1 queries :-/)
      # not too bad in this case, but in time it can become...

      # instead, let's load via Transaction model and avoid multiple queries..
      Transaction.debtor(@user.id)
    end
  end
end
