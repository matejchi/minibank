# frozen_string_literal: true

module Transactions
  class Credited
    def initialize(user)
      @user = user
    end

    def call
      credited_transactions
    end

    private

    def credited_transactions
      # we could easily return transactions by doing just this:
      #   @user.account.debited_transactions
      # but that will trigger 3 additional SQL queries against DB (n+1 queries :-/)
      # not too bad in this case, but in time it can become...

      # instead, let's load via Transaction model and avoid multiple queries..
      Transaction.creditor(@user.id)
    end
  end
end
