# frozen_string_literal: true

module Users
  class Create
    def initialize(params)
      @params = params
    end

    def call
      ActiveRecord::Base.transaction do
        create_user
        create_account
      end
      @user
    end

    private

    def create_user
      @user = User.create!(@params)
    end

    def create_account
      @user.create_account!(description: 'Generated on user create',
                            account_type: 'INTERNAL',
                            currency: 'EUR')
    end
  end
end
