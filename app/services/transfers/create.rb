# frozen_string_literal: true

module Transfers
  class Create
    def initialize(params: _params, debtor: _debtor, creditor: _creditor)
      @params = params
      @debtor = debtor.account
      @creditor = creditor
      @transfer_type = params.dig(:transfer_type)&.downcase
      @amount = BigDecimal(params.dig(:amount).to_s)
    end

    def call
      send("create_#{@transfer_type}_transfer")
    end

    private

    # 'external' transfer type refers to task '4) Via the console you can give the user credit.'
    # won't check balance for 'external' transfers, because this is not real-case scenario
    def create_external_transfer
      ActiveRecord::Base.transaction do
        # check if user is adding credit to his account..
        # only 'external' user can make this kind of transfer (give user a credit)
        raise MiniBankErrors::NotExternalUser if @debtor.description != 'EXTERNAL ACCOUNT'

        create_transaction
        update_creditors_balance
      end
    end

    # 'internal' transfer type, refers to '6) Users can transfer money to each other.'
    def create_internal_transfer
      ActiveRecord::Base.transaction do
        check_balance
        create_transaction
        update_debtors_balance
        update_creditors_balance
      end
    end

    # for the sake of the task (4), as debtor account I'm using 'external' user,
    # since there is no clear specification on how should I give user the credit via console.
    # so in order to respect tasks 8 and 9, I'll use external/'god user' as a debtor, so we
    # can track who made a transfer
    def create_transaction
      Transaction.new.tap do |tr|
        tr.debtor = determine_debtor
        tr.creditor = @creditor
        tr.amount = @amount
        tr.currency = @params.dig(:currency)
        tr.subject = @params.dig(:subject)
        tr.description = @params.dig(:description)
        tr.transaction_type = @transfer_type
        tr.save!
      end
    end

    def determine_debtor
      @transfer_type == 'external' ? gods_account : @debtor
    end

    def gods_account
      User.god_mode.includes(:account).first.account
    end

    def update_creditors_balance
      balance = BigDecimal(@creditor.balance)
      @creditor.update_attributes!(balance: balance + @amount)
    end

    def check_balance
      balance = BigDecimal(@debtor.balance)
      raise MiniBankErrors::NoAvailableFunds unless balance - @amount >= 0
    end

    def update_debtors_balance
      balance = BigDecimal(@debtor.balance)
      @debtor.update_attributes!(balance: balance - @amount)
    end
  end
end
