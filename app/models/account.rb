# frozen_string_literal: true

class Account < ApplicationRecord
  # associations
  belongs_to :user

  # debited transactions are those where account was charged
  has_many :debited_transactions, class_name: 'Transaction', foreign_key: 'debtor_id'

  # credited transactions are those where account was 'payed'
  has_many :credited_transactions, class_name: 'Transaction', foreign_key: 'creditor_id'

  # validations
  validates :account_no, length: { is: 8 }, uniqueness: true, allow_nil: true
  validates :balance, numericality: { greater_than_or_equal_to: 0.0 }

  # callbacks
  before_create :generate_account_no

  private

  def generate_account_no
    self.account_no = loop do
      account = (1..9).to_a.sample(8).join
      break account unless Account.exists?(account_no: account)
    end
  end
end
