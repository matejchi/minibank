# frozen_string_literal: true

class Transaction < ApplicationRecord
  # associations
  belongs_to :debtor, class_name: 'Account'
  belongs_to :creditor, class_name: 'Account'

  # validations
  validates :amount, numericality: { greater_than_or_equal_to: 0.0 }
  validates :subject, presence: true

  # scopes
  scope :debtor, ->(debtor_id) { where(debtor_id: debtor_id) }
  scope :creditor, ->(creditor_id) { where(creditor_id: creditor_id) }
end
