# frozen_string_literal: true

class User < ApplicationRecord
  has_secure_password

  EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i.freeze
  EXTERNAL_USER_EMAIL = 'external_user@isp.net'

  # associations
  has_one :account, dependent: :nullify

  # validations
  validates :first_name, :last_name, :email, presence: true
  validates :email, format: EMAIL_REGEX, uniqueness: true
  validates :password, presence: true, length: { within: 4..15 }, if: :validate_password?
  validates :api_token, uniqueness: true, allow_nil: true

  # callbacks
  before_create :generate_api_token, :prepare_email

  # scopes
  scope :god_mode, -> { where(email: EXTERNAL_USER_EMAIL) }

  def generate_new_token
    generate_api_token
  end

  private

  def generate_api_token
    self.token_expires_at = 1.hour.from_now
    self.api_token = loop do
      token = SecureRandom.urlsafe_base64
      break token unless User.exists?(api_token: token)
    end
  end

  def prepare_email
    email.strip!
    email.downcase!
  end

  def validate_password?
    new_record? || !password.blank?
  end
end
