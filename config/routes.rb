# frozen_string_literal: true

require 'api_constraints'

Rails.application.routes.draw do

  root to: 'home#index', via: :all

  #API
  namespace :api, defaults: { format: 'json'} do
    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true) do
      resources :sessions, only: :create
      resource :session, only: :destroy
      resources :users, only: :create
      resources :transfers, only: :create
      resources :accounts, only: :index

      get 'transactions/credited', to: 'transactions#credited'
      get 'transactions/debited', to: 'transactions#debited'
    end
  end
end
