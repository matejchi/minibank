# README

Ruby ver: 2.5.0
Rails ver: 5.2.1
PostgreSql 9.x

Setup:
 rails db:create
 rails db:migrate
 rails db:seed
 rails s

To run specs, run:
 bundle exec rspec

API documentation can be seen here: https://documenter.getpostman.com/view/5727281/RzZ1rhxy

any questions?

ciloid@gmail.com
